WORKSHOP_NAME=lab-k8s-fundamentals
WORKSHOP_IMAGE=quay.io/openshiftlabs/lab-k8s-fundamentals:master
RESOURCE_BUDGET=medium
MAX_SESSION_AGE=5400
IDLE_TIMEOUT=300
